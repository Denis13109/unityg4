using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterMotor : MonoBehaviour
{
    Animation animations;

    public float walkSpeed;
    public float runSpeed;
    public float turnSpeed;

    public string inputFront;
    public string inputBack;
    public string inputLeft;
    public string inputRight;
    public string inputSprint;

    public Vector3 jumpSpeed;
    CapsuleCollider playerCollider;

    // Start is called before the first frame update
    void Start()
    {
        animations = gameObject.GetComponent<Animation>();
        playerCollider = gameObject.GetComponent<CapsuleCollider>();
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKey(inputFront) && !Input.GetKey(inputSprint))
        {
            transform.Translate(0, 0, walkSpeed * Time.deltaTime);
            animations.Play("Run");
        }

         if(Input.GetKey(inputFront) && Input.GetKey(inputSprint))
        {
            transform.Translate(0, 0, runSpeed * Time.deltaTime);
            animations.Play("Run");
        }

        if(Input.GetKey(inputBack))
        {
            transform.Translate(0, 0, -(walkSpeed / 2) * Time.deltaTime);
            animations.Play("Run");
        }

          if(Input.GetKey(inputLeft))
        {
            if(Input.GetKey(inputLeft) && Input.GetKey(inputFront)){
                transform.Translate(0, 0, runSpeed * Time.deltaTime);
                transform.Rotate(0, -turnSpeed * Time.deltaTime, 0);
                animations.Play("Run");
            }
            transform.Rotate(0, -turnSpeed * Time.deltaTime, 0);
        }

        if(Input.GetKey(inputRight))
        {
            transform.Rotate(0, turnSpeed * Time.deltaTime, 0);
        }
    }
}
