using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class newCharacter : MonoBehaviour
{
    // Start is called before the first frame update
    public float walkSpeed;
    public float runSpeed;
    public float turnSpeed;

    public string inputFront;
    public string inputBack;
    public string inputLeft;
    public string inputRight;

    private Animator anim;

    void Start()
    {
        anim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        //marche
        if(Input.GetKey(inputFront))
        {
            transform.Translate(0, 0, walkSpeed * Time.deltaTime);
            anim.SetBool("walk", true);
        }else if (Input.GetKey(inputBack)){
            transform.Translate(0, 0, - (walkSpeed/2) * Time.deltaTime);
            anim.SetBool("walk", true);
        } else if (Input.GetKey(inputLeft)) {
            transform.Rotate(0, -turnSpeed * Time.deltaTime, 0);
        } else if (Input.GetKey(inputRight)) {
            transform.Rotate(0, turnSpeed * Time.deltaTime, 0);
        } else {
            anim.SetBool("walk", false);
        }

        //back
        
        

        /*transform.Translate(Vector3.forward * Input.GetAxis("Vertical") * VitesseMarche * Time.deltaTime);
        transform.Rotate(Vector3.up * Input.GetAxis("Horizontal") * CoefRot * Time.deltaTime);*/
    }
}
